\section{Architecture}
\label{sec:architecture}

Kalibro Metrics was designed for easy integration with different metric collector
tools, source code repositories, and user interfaces.
%
Under Kalibro repository there are 7 sub-projects, as listed below:

\begin{enumerate}
	\item \textbf{KalibroCore} is the heart of Kalibro Metrics. It contains
all logic, including database access, metric collector tool interaction,
statistics computation and compound metric scripts evaluation. It includes a
Java abstract class (\verb|KalibroFacade|) to access all its functionality.

	\item \textbf{KalibroDesktop} is a swing interface. It contains
basically components and controllers which communicate with \verb|KalibroFacade|.

	\item \textbf{KalibroService} is a Web Service interface for
using Kalibro.

	\item \textbf{KalibroSpago} is the Kalibro Extractor for
Spago4Q~\cite{spago4q}.

	\item \textbf{KalibroTests} contains all Kalibro automated tests.

	\item \textbf{Libraries} contains Kalibro dependencies.

	\item \textbf{Resources} contains documents, development guides,
scripts and other utilities.

\end{enumerate}

Kalibro can work in 2 ways: accessing KalibroService or KalibroCore
directly.
%
Thus, it is possible to use Kalibro features on different graphical user
interfaces with a very lightweight installation and almost no configuration.
%
Via Web Service, the download of source code, processing, running the metric
collector tools and persistence takes place on the server.

%%AVISO: a figura da KalibroCore foi colocada nos trabalhos relacionados para
%% aparecer antes, pq ela ocupa duas colunas
Figure~\ref{fig:kalibrocore} shows an overview of Kalibro architecture, presenting
its most important components.
%
All internal logic remains on KalibroCore. The Java class \verb|KalibroFacade|
was designed to serve as the entry point to the system.
%
Thus, the only types to be used outside KalibroCore are \verb|KalibroFacade|
and its interface dependencies.
%
There are 2 implementation classes for \verb|KalibroFacade|:
\verb|KalibroLocal|, which answers the requests directly, and
\verb|KalibroClient|, which transfers requests to a remote Kalibro Service.
%
\verb|KalibroFacade| statically instantiate one or the other according to
integration with graphical interface (view layer) and its installation settings.

The download and analysis of a project executes asynchronously. To be notified
when both steps are completed, it is necessary to register a
\verb|ProjectStateListener|.
%
A project listener is notified when the state of a project changes. The project
state indicates its progress in this process. There are 5 different states:

\begin{enumerate}
	\item \textbf{Unknown:} There is no such project in the database. It was
deleted or never was saved.

	\item \textbf{Loading:} The software source code is being loaded. The files are
obtained from the project repository and all non-code file is removed
(according to the project language and file extension).

	\item \textbf{Analyzing:} The project is being analyzed. The metric collector
tool runs on the loaded code, then Kalibro computes statistics.

	\item \textbf{Ready:} The results are available.

	\item \textbf{Error:} An error occurred while processing the project. A project
in error state has an error report, which contains the state it was when the
error occurred and the stack trace of the exception.

\end{enumerate}

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.37\textwidth]{images/core_concurrent_overview.png}
  \end{center}
  \caption{org.kalibro.core.concurrent package overview}
  \label{fig:core_concurrent}
\end{figure}
%\vspace{-0.5em}

The interface \verb|MetricCollector| is how Kalibro interacts with the metric
collector tools.
%
KalibroCore includes a \verb|MetricCollector| implementation for running Analizo
(\verb|AnalizoMetricCollector|), but this interface can be implemented to call
any metric collector tool. There are only 3 methods to implement:
\vspace{1em}

\begin{itemize}
	\item \verb|getSupportedMetrics()| specifies which metrics the tool supports.

	\item \verb|getSupportedLanguages()| specifies which programming
languages the tool is able to analyze.

	\item \verb|collectMetrics(codePath, language)| actually collects the
metric results.
\end{itemize}

All thread management is encapsulated in the package
\verb|org.kalibro.core.concurrent| (see Figure~\ref{fig:core_concurrent}).
%
Tasks are extensions of the abstract class \verb|Task|. A \verb|TaskListener|
can be registered to be notified when the tasks finishes.
%
A finished task generate a \verb|TaskReport|, which informs if the task has
completed normally (generating eventual results) or has crashed
(capturing the exception).
%
The \verb|TaskExecutor| class contains methods for running tasks synchronously,
asynchronously, periodically and with timeouts.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=.45\textwidth]{images/CommandExecutor_overview.png}
  \end{center}
  \caption{CommandExecutor class overview}
  \label{fig:CommandExecutor}
\end{figure}
%\vspace{-0.5em}

The class \verb|CommandExecutor| is used for executing commands
(see Figure~\ref{fig:CommandExecutor}). Every command
executed generates a log file for recording its error output. According to the
method used, the standard output can also be logged or just returned as stream.
%
Kalibro loads source code from repository by executing system calls. Adding a
new repository type is just a matter of specifying the String to run on command
line.

In addiction, Kalibro uses JPA (Java Persistence API) to persist its entities.
With little configuration, it can be installed for using any database which
supports JPA.
%
The package \verb|org.kalibro.core.processing| (see Figure~\ref{fig:kalibrocore})
contains the algorithms for building the project source tree, calculating
statistics and evaluating compound metric scripts.

Finally, the Kalibro architecture was developed to be well modularized,
providing an organized source code to receive contributions from
Kalibro Free Software community.
%
In summary, it was designed prioritizing the support to the main
Kalibro characteristics and differential: multi-repository and
multi-language source code analysis.    
