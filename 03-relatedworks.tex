\section{Related works}
\label{sec:related-work}

During our research and development activities, we studied or used 11
free software source code analysis tools:
%
Analizo~\cite{analizo},
CCCC~\cite{cccc},
Checkstyle~\cite{checkstyle},
CMetrics~\cite{cmetrics},
CPPX~\cite{hassan2005},
Cscope~\cite{cscope}
CTAGX~\cite{hassan2005},
LDX~\cite{hassan2005}
JaBUTi~\cite{jabuti},
MacXim~\cite{macxim}
and Metrics (Eclipse plug-in)~\cite{eclipsemetrics}.


Our group has been researching source code quality from both
theoretical and practical perspectives.
%
Also, we have been working with empirical studies based in source code analysis.
%
We have searched for source code assessment tools to test our
conceptual hypotheses and support our practical tasks.
%
Thus, we have defined the following requirements for our source code metrics
tool:

\begin{enumerate}

  \item \textbf{Thresholds:}
    The tool should support source code metrics thresholds to provide different
interpretations about metric values. Users should be able to choose a set of
thresholds according to the language or implementation context. 


  \item \textbf{Multi-language:}
    The tool should support the analysis of different programming languages
since this can attract different potential users who could suggest thresholds, 
according to their experiences with specific languages.
% 
Also, this can enhance the scope and validity of our studies.
%
At the moment, we wanted one that could support source code analysis
for C, C++, and Java programs.

  \item \textbf{Extensibility:}
    The tool should provide clear interfaces for adding new metrics and supporting
different programming language or connecting with other source code analysis
tools to promote multi-language support.

  \item \textbf{Free Software:}
    The tool should be free software, available without restrictions. Thus,
we can contribute within its development according to our needs. Also, this allows
other researchers to replicate our studies and results fully.

  \item \textbf{Actively maintained:} 
    The tool should be supported by active developers who know the tool
architecture to provide updates, fix bugs, and interact with us to improve it.

 
\end{enumerate}


\begin{table}[htb]
  \centering
%  \scalefont{0.95}
  \begin{tabular}{|l|c|c|c|c|}
    \hline
    \textbf{Tools} &
    \textbf{Languages} & %Multi-language
    \textbf{Extensible} & %Extensibility
    \textbf{Thresholds} & % To Support Thresholds
    \textbf{Maintained} \\\hline\hline %Actively maintained 
				    
    Analizo 	& C, C++, Java & Yes & No  & Yes    \\\hline
    CCCC 	& C++, Java    & No  & No  & Yes    \\\hline
    Checkstyle	& Java         & No  & Yes & Yes    \\\hline
    CMetrics	& C            & Yes & No  & Yes    \\\hline
    CPPX	& C, C++       & No  & No  & No     \\\hline
    Cscope	& C            & No  & No  & Yes    \\\hline
    CTAGX	& C            & No  & No  & No     \\\hline
    LDX		& C, C++       & No  & No  & No     \\\hline
    JaBUTi	& Java         & No  & No  & Yes    \\\hline
    MacXim	& Java         & No  & No  & Yes    \\\hline
    Metrics 	& Java         & No  & Yes & Yes    \\\hline
    
  \end{tabular}


  \caption{Existing tools versus defined requirements}
  \label{tab:tools}
%  \scalefont{1}
\end{table}
\vspace{-1em}

In Table \ref{tab:tools}, we compare all of these tools to our requirements.
%
None of the tools that we have studied fulfill all of our requirements.
%
Although the Analizo tool is indicated as able to analyze source code from our
initial three required languages (C, C++, and Java), at first moment,
it did not support the Java language.  
%
Because Analizo has documented extension interfaces, we could help its
software engineers to implement the support to Java and new metrics.


At that time, we started the development of Kalibro Metrics to integrate it
with Analizo.
%
We want a tool with thresholds support, and Analizo metric tool
needed a graphical interface and a friendlier approach.
%
Thus, Analizo acts as the Kalibro default source code analysis tool since Kalibro
Metrics supports different source code collector tools.

In addition, we chose the Analizo metric tool because it has an efficient C, C++,
and Java source code parse and supports the extraction of a large number of
source code metrics \cite{analizo}.
%
To have an easy-to-use tool which provided information for multi-language
source code, Analizo was designed to support the use of external tools as
extractors. Both Analizo and each extractor are command line based applications,
accepting files or directories as inputs and reporting source code information
as output.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{images/analizo_doxyparse_workflow.png}
  \end{center}
  \caption{Analizo workflow using Doxyparse as extractor}
  \label{fig:analizoworkflow}
\end{figure}


Analizo is written in Perl using the object-oriented
paradigm, providing both flexibility and efficiency. Doxyparse is the main
extractor capable of parsing C, C++ and Java and grants great performance.
%
It is based on Doxygen, a widely used Free Software tool for the generation of
documentation through the code created in different programming
languages~\cite{doxygen}.
%
Figure~\ref{fig:analizoworkflow} presents an example of the workflow when
Analizo uses Doxyparse as extractor.


%Figura abaixo faz parte da outra secao, colocada aqui para aparecer antes.
%TODO: pensar em numerar na figura as partes que são citadas no texto.
\begin{figure*}[htpb]
  \begin{center}
    \includegraphics[width=.9\textwidth]{images/KalibroCore_Overview.png}
  \end{center}
  \caption{KalibroCore architecture overview}
  \label{fig:kalibrocore}
\end{figure*}
%\vspace{-1em}

For example, Kalibro Metrics provides a source code path as an input via the
command line to Analizo, which executes Doxyparse based on Doxygen internals.
%
Doxygen parses all input files and stores a source code representation on its
data structures.
%
Afterwards, Doxyparse ends its execution by reporting information concerning
modules/classes, functions/methods and attributes/variables found.
%
By the end of the Doxyparse output process, Analizo contains all the
information necessary to calculate the metrics.
%
The last step of the work-flow presented in Figure \ref{fig:analizoworkflow}
is the one which Analizo calculates the metrics.


Kalibro integrated with Analizo already provides a multi-language approach, but
according to the reasons to create it, Kalibro needs to support other programming
languages.
%
Analizo is multi-language and can support more
languages through Doxyparse extensions because of Doxygen features.


However, as we have observed during the development of Java support, the
multi-language approach may constrain the implementation of some metrics we 
need.
%
In other words, there are specific metrics from object-oriented paradigm or
languages that we had difficulties programming for Analizo such as, for Java,
number of interfaces, number of packages, comment lines of code, etc.

In this context, it is an interesting differential that Kalibro
Metrics can support distinct source code analysis tools.
%
Thus, we can associate it with a tool according to the source code under analysis.
%
This way, Kalibro can be integrated with the most useful metric tools from each
programming language community.
%
For example, Kalibro Metrics should support Python, Ruby, and PHP because these
languages also are widely used. Its architecture allows their future incorporation.


